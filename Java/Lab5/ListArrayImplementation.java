import java.util.*;


/**
 * Created by olvet on 19/04/2016.
 */
class ListArrayImplementation<E> extends AbstractList<E> implements List<E> {
    private int size;
    private Object[] tempArray;
    int index;

    //Constructor
    public ListArrayImplementation() {
        tempArray = new Object[10];

    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {
        boolean s = false;
        if (size == 0) {
            s = true;
        }
        return s;
    }

    @Override
    public boolean contains(Object o) {
        boolean x = false;
        for (int i = 0; i < size; i++) {
            if (tempArray[i].equals(o) == true) {
                x = true;
                break;

            }

        }
        return x;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyItr() ;
    }


    @Override
    public boolean add(Object o) {
        if (index == tempArray.length) {
            Object[] newArray = new Object[tempArray.length * 2];
            System.arraycopy(tempArray, 0, newArray, 0, index - 1);
            tempArray = newArray;
        }
        tempArray[index] = o;
        index++;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (int index = 0; index < size; index++)
                if (tempArray[index] == null) {
                    int num = size - index - 1;
                    if (num > 0)
                        System.arraycopy(tempArray, index + 1, tempArray, index,
                                num);
                    tempArray[--size] = null;
                    return true;
                }
        } else {
            for (int index = 0; index < size; index++)
                if (o.equals(tempArray[index])) {
                    int num = size - index - 1;
                    if (num > 0)
                        System.arraycopy(tempArray, index + 1, tempArray, index,
                                num);
                    tempArray[--size] = null;
                    return true;
                }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        Iterator e = c.iterator();
        while (e.hasNext())
            if (!contains(e.next()))
                return false;
        return true;


    }

    @Override
    public boolean addAll(Collection c) {
        Object[] o = c.toArray();
        int num = o.length;
        checkSize(size + num);
        System.arraycopy(o, 0, tempArray, size, num);
        size += num;
        return num != 0;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        Object[] o = c.toArray();
        int num = o.length;
        checkSize(size + num);
        int mov = size - index;
        if (mov > 0) System.arraycopy(tempArray, index, tempArray, index + num, mov);
        System.arraycopy(o, 0, tempArray, size, num);
        size += num;
        return num != 0;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean x = false;
        for (int i = 0; i < size; i++) {
            if (c.equals(tempArray[i])) {
                tempArray[i] = null;
                x = true;
            } else x = false;


        }
        return x;

    }

    @Override
    public boolean retainAll(Collection c) {
        boolean x = false;
        for (int i = 0; i < size; i++) {
            if (!c.equals(tempArray[i])) {
                tempArray[i] = null;
                x = true;
            } else x = false;
        }
        return x;

    }


    @Override
    public void clear() {
        for (int i = 0; i < size; i++)
            tempArray[i] = null;
        size = 0;
    }


    @Override
    public E get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
        }
        return (E) tempArray[index];
    }

    @Override
    public Object set(int index, Object element) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
        }
        if (index < size) {
            E oldValue = elements(index);
            tempArray[index] = element;
            return true;
        }
        return false;

    }

    @Override
    public void add(int index, E element) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
        }
        checkSize(size + 1);
        System.arraycopy(tempArray, index, tempArray, index + 1,
                size - index);
        tempArray[index] = element;
        size++;
    }


    @Override
    public E remove(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
        }
        E oldValue = (E) tempArray[index];
        int value = size - index - 1;
        if (value > 0) {
            System.arraycopy(tempArray, index + 1, tempArray, index, value);
        }
        tempArray[--size] = null;
        return oldValue;

    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (tempArray[i] == null) {
                    return i;
                }
            }
            for (int i = 0; i < size; i++) {
                if (tempArray[i].equals(o)) {
                    return i;
                }
            }
        }

        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = size - 1; i >= 0; i--)
                if (tempArray[i] == null)
                    return i;
        } else {
            for (int i = size - 1; i >= 0; i--)
                if (tempArray[i].equals(o))
                    return i;
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {

        return new ListItr(0);

    }

    @Override
    public ListIterator<E> listIterator(final int index) {
        return new ListItr(index);
    }

    private class MyItr implements Iterator<E> {
        int cursor;
        int lastRet = -1;
        int expectedModCount = modCount;

        public boolean hasNext() {
            return cursor != size();
        }

        public E next() {
            checkForComodification();
            try {
                int i = cursor;
                E next = get(i);
                lastRet = i;
                cursor = i + 1;
                return next;
            } catch (IndexOutOfBoundsException e) {
                checkForComodification();
                throw new NoSuchElementException();
            }
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();
            try {
                ListArrayImplementation.this.remove(lastRet);
                if (lastRet < cursor)
                    cursor--;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException e) {
            }
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

    private class ListItr extends MyItr implements ListIterator<E> {
        ListItr(int index) {
            cursor = index;
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public E previous() {
            checkForComodification();
            try {
                int i = cursor - 1;
                E previous = get(i);
                lastRet = cursor = i;
                return previous;
            } catch (IndexOutOfBoundsException e) {
                checkForComodification();
                throw new NoSuchElementException();
            }
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public void set(E e) {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                ListArrayImplementation.this.set(lastRet, e);
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(E e) {
            checkForComodification();

            int i = cursor;
            ListArrayImplementation.this.add(i, e);
            lastRet = -1;
            cursor = i + 1;
            expectedModCount = modCount;

        }
    }


    public List<E> subList(int fromIndex, int toIndex) {

        return new SubList(this, 0, fromIndex, toIndex);
    }

    private class SubList extends AbstractList<E> implements RandomAccess {
        private final AbstractList<E> parent;
        private final int parentOffset;
        private final int offset;
        private int size;

        SubList(AbstractList<E> parent,
                int offset, int fromIndex, int toIndex) {
            this.parent = parent;
            this.parentOffset = fromIndex;
            this.offset = offset + fromIndex;
            this.size = toIndex - fromIndex;

        }

        public E set(int index, E e) {
            rangeCheck(index);

            E oldValue = ListArrayImplementation.this.elements(offset + index);
            ListArrayImplementation.this.tempArray[offset + index] = e;
            return oldValue;
        }

        public E get(int index) {
            rangeCheck(index);

            return ListArrayImplementation.this.elements(offset + index);
        }

        public int size() {

            return this.size;
        }


        public void add(int index, E e) {
            parent.add(parentOffset + index, e);
            this.size++;
        }

        public E remove(int index) {
            rangeCheck(index);
            E result = parent.remove(parentOffset + index);
            this.size--;
            return result;
        }

    }

    public Object[] toArray() {
        return Arrays.copyOf(tempArray, size);
    }

    private void rangeCheck(int index) {
        if (index >= size) throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
    }

    private E elements(int index) {
        return (E) tempArray[index];
    }

    public void checkSize(int minCapacity) {

        int oldCapacity = tempArray.length;
        if (minCapacity > oldCapacity) {
            Object oldData[] = tempArray;
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity < minCapacity)
                newCapacity = minCapacity;
            tempArray = Arrays.copyOf(tempArray, newCapacity);
        }

    }
}

