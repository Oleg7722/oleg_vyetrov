/**
 * Created by olvet on 23.04.2016.
 */

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Calendar;


public class Output {

    public void Initialize(){

    }

    public static void main(String[] args) {
        ListArrayImplementation<Long> mylist = new ListArrayImplementation<>();
        for (int i = 0; i < 30; i++) {
            long time = (long) (Math.random() * 1461241670);
            mylist.add(i, time);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Calendar calendar = Calendar.getInstance();
        int day = 0;
        for (int i = 0; i < 30; i++) {
            Date date = new Date(mylist.get(i) * 1000);
            calendar.setTime(date);
            try {
                day = calendar.get(Calendar.DAY_OF_WEEK);


                if ((day == Calendar.MONDAY) || (day == Calendar.TUESDAY) || (day == Calendar.WEDNESDAY) || (day == Calendar.THURSDAY) || (day) == Calendar.FRIDAY) {
                    throw new WorkingDaysRunTimeException();
                }
                if ((day == Calendar.SATURDAY) || (day == Calendar.SUNDAY)) {
                    throw new WeekDaysException();
                }
            } catch (WorkingDaysRunTimeException e) {
                String stringDate = sdf.format(date);
                System.out.println(stringDate + " " + "was" + " " + DaysOfWeekToString.printDaysOfWeek(day) + ". It was working day");

            } catch (WeekDaysException e) {
                String stringDate = sdf.format(date);

                System.out.println(stringDate + " " + "was" + " " + DaysOfWeekToString.printDaysOfWeek(day) + ". It was week day");

            }


        }


    }
}