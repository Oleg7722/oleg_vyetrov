/**
 * Created by olvet on 23.04.2016.
 */
public class DaysOfWeekToString {
    public static String printDaysOfWeek(int day) {
        String x = "";
        if (day == 1) {
            x = "SUNDAY";
        } else if (day == 2) {
            x = "MONDAY";
        } else if (day == 3) {
            x = "TUESDAY";
        } else if (day == 4) {
            x = "WEDNESDAY";
        } else if (day == 5) {
            x = "THURSDAY";
        } else if (day == 6) {
            x = "FRIDAY";
        }
        if (day == 7) {
            x = "SATURDAY";
        }

        return x;
    }

}
