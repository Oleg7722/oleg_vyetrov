/**
 * Created by Oleg on 12.04.2016.
 * Outputs even numbers from [0;99]  range
 */
public class EvenNumberOutput {
    static public void main(String args[]) {
        System.out.println("   " + "Even numbers from [0;99]  range");
        for (int i = 2; i < 99; i = i + 2) {       // Outputs even numbers from [0;99]  range
            System.out.println(i);

        }
    }
}
