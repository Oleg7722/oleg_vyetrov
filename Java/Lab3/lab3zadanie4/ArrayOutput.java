/**
 * Created by Oleg on 12/04/2016.
 * Initializes array with random number from [-100;100] range and then outputs array by index
 */
public class ArrayOutput {
    static public void main(String args[]) {
        int[] arr = new int[4];
        System.out.println("   " + "Your array is:");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) ((Math.random() * 201) - 100); // Initializes array with random number from [-100;100] range
            System.out.println("arr[" + i + "]" + "=" + arr[i]); //Outputs array by index
        }

    }
}
