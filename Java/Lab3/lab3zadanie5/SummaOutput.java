/**
 * Created by Oleg on 17.04.2016.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class SummaOutput {
    public static void main(String[] args) {
        double term = 0;
        double sum = 0;
        String inputValue = new String();
        boolean flag = false;
        do {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the term or the words" + " " + "Summa/summa to finish");
                inputValue = br.readLine();
            } catch (IOException e) {
                System.err.println("Error: " + e);
            }
            try {
                term = Double.parseDouble(inputValue);
                sum = term + sum;
                System.out.println();
                flag = true;
            } catch (NumberFormatException e) {
                if (!inputValue.equals("Summa") && (!inputValue.equals("summa"))) {
                    System.out.println("This not a number, please try once again.");

                }
            }
        } while (!inputValue.equals("Summa") && (!inputValue.equals("summa")));
        System.out.println("Your sum is" + " " + sum);


    }
}
