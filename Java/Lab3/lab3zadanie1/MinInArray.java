/**
 * Created by Oleg on 13.04.2016.
 * Finds index and value of array min
 */
public class MinInArray {
    public static void main(String[] args) {
        int[] arr = new int[4];
        System.out.print("Your array is");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) ((Math.random() * 201) - 100); // Initializes array with random number from [-100;100] range
            System.out.print(" " + arr[i] + " ");
        }
        int imin = 0;
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {  //Finds index and value of min
            if (arr[i] < min) {
                imin = i;
                min = arr[i];
            }
        }
        System.out.println();
        System.out.println("Min array number=" + arr[imin]);
    }
}
