/**
 * Created by olvet on 13/04/2016.
 * Computes Prime Number from given range by optimized algorithm and non-optimized algorithm and shows time for each of them.
 */

import java.util.Scanner;

public class PrimeNumberOutput {
    static public void main(String args[]) {


        System.out.println("Enter N to set the range [2; N] for  Prime Number search");
        boolean flag = true;
        int n = 0;
        do
        {                                               // Gets input from user , checks that N is in range, and assigns to N
            Scanner sc = new Scanner(System.in);
            if (sc.hasNextInt()) {
                n = sc.nextInt();
                flag = false;
            } else {
                System.out.println("It is not an Integer, please try again!");
                flag = true;
            }
            if ((n < 2) && (flag == false)) {
                System.out.println("It is less than 2, please try again!");
                flag = true;
            }

        } while (flag == true);
        /** Optimized algorithm for Prime Number computing. Shows time taken by calculation
         *
         */
        System.out.println("    " + "Prime Numbers computed by optimized algorithm");
        System.out.println();
        long timestart = System.currentTimeMillis(); // Start time for optimized algorithm
        if (n == 2) {
            System.out.println("The Prime Numbers from your range [1; 2] is: \n 2"); // Output for  Prime Number 2 if N=2

        } else {
            System.out.println("The Prime Numbers from your range [1;" + n + "]" + "is: \n" + "2");
            /* Calculates only for odd dividend and odd divider until divider less than square root from dividend  */
            for (int i = 3; i <= n; i = i + 2) {
                double ost = 0;
                for (int j = 3; j < i; j = j + 2) {

                    if (Math.sqrt(i) < j) {
                        break;
                    }
                    if ((i % j) == 0) {
                        ost = 1;
                        break;
                    }
                }


                if (ost != 1) {
                    System.out.println(i);
                }

            }

        }
        long timefinish = System.currentTimeMillis(); // End time for optimized algorithm
        long timeoptimizedalgorithm = timefinish - timestart; // Computes how much time  optimized algoritm take
        System.out.println("Compute time for optimized algorithm=" + timeoptimizedalgorithm + " " + "millis");

        long timestartnon = System.currentTimeMillis(); // Start time for non-optimized algorithm
        /** Non optimized algorithm for Prime Number computing. Shows time taken by calculation
         *
         */
        for (int i = 2; i <= n; i++) {

            double ost = 0;
            for (int j = 2; j < i; j++) {


                if ((i % j) == 0) {
                    ost = 1;

                }
            }


            if (ost != 1) {

            }

        }
        long timefinishnon = System.currentTimeMillis(); // End time for non optimized algorithm
        long timeoptimizedalgorithmnon = timefinishnon - timestartnon; // Computes how much time  non-optimized algoritm take
        System.out.println();
        System.out.println("Compute time for non-optimized algorithm=" + timeoptimizedalgorithmnon + " " + "millis");
    }
}














