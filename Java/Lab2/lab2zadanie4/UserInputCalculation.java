import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * Created by Oleg on 10.04.2016.
 * Calculates Arithmetic values using UserInputs fields
 */
public class UserInputCalculation {
    public static void main(String args[]) {
        /* Initializes UserInput  object and sets fields for loop
         *
         */
        UserInput maths = new UserInput();
        String inputValue1;
        String flag = "no";
        do {                          //Makes calculations in loop, until user abort
            maths.setFirstArgument(); // Sets UserInput fields
            maths.setArithmeticOperator();
            maths.setSecondArgument();

            String Operator = maths.getArithmeticOperator(); // Assigns Operator field to String object
            if (Operator.equals("+")) { //Block performs arithmetic operations
                double sum = 0;
                sum = maths.getFirstArgument() + maths.getSecondArgument();
                System.out.println("Sum=" + maths.getFirstArgument() + "+" + maths.getSecondArgument() + "=" + sum);
            } else if (Operator.equals("-")) {
                double sum = 0;
                sum = maths.getFirstArgument() - maths.getSecondArgument();
                System.out.println("Sum=" + maths.getFirstArgument() + "-" + maths.getSecondArgument() + "=" + sum);
            } else if (Operator.equals("*")) {
                double sum = 0;
                sum = maths.getFirstArgument() * maths.getSecondArgument();
                System.out.println("Sum=" + maths.getFirstArgument() + "*" + maths.getSecondArgument() + "=" + sum);
            }


            while ((maths.getSecondArgument() == 0) && (Operator.equals("/"))) { //Checks division by zero
                System.out.println("Division by 0 is forbidden! Please enter another second number");
                maths.setSecondArgument();
            }
            while ((maths.getSecondArgument() == 0) && (Operator.equals("%"))) { //Checks division by zero
                System.out.println("Division by 0 is forbidden! Please enter another second number");
                maths.setSecondArgument();
            }
            if (Operator.equals("/")) { // Divides if no division by zero
                double sum = 0;
                sum = maths.getFirstArgument() / maths.getSecondArgument();
                System.out.println("Sum=" + maths.getFirstArgument() + "/" + maths.getSecondArgument() + "=" + sum);
            }
            if (Operator.equals("%")) { // Divides if no division by zero
                double sum = 0;
                sum = maths.getFirstArgument() % maths.getSecondArgument();
                System.out.println("Sum=" + maths.getFirstArgument() + "%" + maths.getSecondArgument() + "=" + sum);
            }
            System.out.println();
            System.out.println("Do you want to repeat? Y/N?");


            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // Reads user answer to continue
                inputValue1 = br.readLine();
                if ((inputValue1.equals("Y")) || (inputValue1.equals("y"))) {
                    flag = "yes";
                } else {
                    flag = "no";

                }
            } catch (IOException e) {
                System.err.println("Error: " + e);
            }
        } while (flag == "yes");


    }
}
