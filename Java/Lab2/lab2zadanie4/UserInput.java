/**
 * Created by Oleg on 10.04.2016.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * Takes user inputs, checks it and assigns them to fields;
 */

public class UserInput {
    private double firstArgument;


    private String arithmeticOperator;
    private double secondArgument;


    public void setFirstArgument() { //Setter for firstArgument
        String inputValue = new String();
        boolean flag = false;
        do {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the first  number:");
                inputValue = br.readLine();
            } catch (IOException e) {
                System.err.println("Error: " + e);
            }
            try {
                firstArgument = Double.parseDouble(inputValue);
                System.out.println("The first number you entered:" + " " + firstArgument);
                System.out.println();
                flag = true;
            } catch (NumberFormatException e) {
                System.out.println("This not a number, please try once again.");
            }
        } while (flag != true);
        //System.out.println("firstArgument=" + " " + firstArgument); Just for testing firstArgument field
    }


    public void setArithmeticOperator() { //Setter for arithmeticOperator
        String inputValue1 = new String();
        boolean flag = false;
        System.out.println("Enter the arithmetic operator:");
        do {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                inputValue1 = br.readLine();
                if ((inputValue1.equals("+")) || (inputValue1.equals("-")) || (inputValue1.equals("*")) || (inputValue1.equals("/")) || (inputValue1.equals("%"))) {
                    arithmeticOperator = inputValue1;
                    flag = true;
                } else {
                    System.out.println("You entered not  the arithmetic operator, please try again");

                }
            } catch (IOException e) {
                System.err.println("Error: " + e);


            }

        } while (flag != true);
        System.out.println("You entered arithmetic operator=" + " " + arithmeticOperator);
        System.out.println();

    }

    public void setSecondArgument() { // Setter for secondArgument
        String inputValue2 = new String();
        boolean flag = false;
        do {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the second number:");
                inputValue2 = br.readLine();
            } catch (IOException e) {
                System.err.println("Error: " + e);
            }
            try {
                secondArgument = Double.parseDouble(inputValue2);
                System.out.println("The second number you entered:" + " " + secondArgument);
                flag = true;
            } catch (NumberFormatException e) {
                System.out.println("This not a number, please try once again.");
            }
        } while (flag != true);

        // System.out.println("secondArgument=" + " " + secondtArgument); Just for testing secondtArgument
    }

    public double getFirstArgument() { //Getter of firstArgument
        return firstArgument;
    }

    public String getArithmeticOperator() { // Getter of arithmeticOperator
        return arithmeticOperator;
    }

    public double getSecondArgument() { // Getter of secondArgument
        return secondArgument;
    }

}

