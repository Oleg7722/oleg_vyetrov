/**
 * Created by Oleg on 09.04.2016.
 */
 public   class lab2zadanie123 {
    public static void main(String[] args) {
        /** Variables declaration for the tasks
         *  Just another comment for filling line
         */
        int a=1555;
        int b=102;
        double  x=2.23E5;
        float y=-45.0456f;
        String str1="Java";
        String str2=new String("Is is not a coffee");
        char c1='x';
        char c2='М';
        boolean yes=true;
        boolean no=false;
        /** Beginning of Task 1
         *  Using of operators
         */
        int z1=a+b;                           // Arithmetic Operators
        System.out.println(" "+" "+" "+"Task 1");
        System.out.println("z1="+z1);
        int z2=a-b;
        System.out.println("z2="+z2);
        int z3=a%b;
        System.out.println("z3="+z3);
        int z4=a-b;
        System.out.println("z4="+z4);
        double w1=x+y;
        System.out.println("w1="+w1);
        double w2=x-y;
        System.out.println("w2="+w2);
        double w3=x%y;
        System.out.println("w3="+w3);
        double w4=x-y;
        System.out.println("w4="+w4);
        String str3=str1+str2;
        System.out.println("str3="+""+str3);
        String str4=str1+a;
        System.out.println("str4="+str4);
        if (a>b) {                                   // Equality, Relational, and Conditional Operators
            System.out.println("a bigger than b");
        }  else {
            System.out.println("b bigger than a");
          }
        if (x!=y) {
                System.out.println("x is not equal to y");
        }
        if ((a>b) && (x>y)) {                          //
                    System.out.println("b bigger than a");
            System.out.println("Both conditions are true");
        }    else if ((yes != true) || (no != true)) {
            System.out.println("This condition never run");
        }

        if(a==b) {
            System.out.println("a is equal to b\n");
        } else {
            System.out.println("a is not equal to b\n");
        }
        /** Beginning of Task 2
         *  Converting Numbers to  Strings
         */
        String integerToString=String.valueOf(a);
        String doubleToString=Double.toString(x);
        String floatToString=Float.toString(y);
        String characterToString=String.valueOf(c2);
        System.out.println(" "+" "+" "+"Task 2");
        System.out.println("Converting Numbers to  Strings");
        System.out.println("integerToString="+""+integerToString);
        System.out.println("doubleToString="+""+doubleToString);
        System.out.println("floatToString="+""+floatToString);
        System.out.println("characterToString="+""+characterToString);
        /* Converting Strings to Numbers and Booleans */
        byte stringToByte=0;
        String str5="100";
        stringToByte=Byte.parseByte(str5);
        System.out.println("Strings to Numbers and Booleans");
        System.out.println("stringToByte="+""+stringToByte);
        String str6="1454";
        int stringToInteger= Integer.parseInt(str6);
        System.out.println("stringToInteger="+""+stringToInteger);
        String str7="564.6e10";
        double stringToDouble=Double.parseDouble(str7);
        System.out.println("stringToDouble="+""+stringToDouble);
        String str8="0.655e-5";
        float stringToFloat=Float.parseFloat(str8);
        System.out.println("stringToFloat="+""+stringToFloat);
        String str9="true";
        boolean stringToBoolean=Boolean.parseBoolean(str9);
        System.out.println("stringToBoolean="+""+stringToBoolean);
        String str10="1";
        stringToBoolean=Boolean.parseBoolean(str10);
        System.out.println("stringToBoolean="+""+stringToBoolean+"\n");
        /** Beginning of Task 3
         *  Converting Numbers to  Strings
         */
        int doubleToInteger=(int) x;
        System.out.println(" "+" "+" "+"Task 3");
        System.out.println("doubleToInteger="+""+doubleToInteger);
        int floatToInteger=(int) y;
        System.out.println("floatToInteger="+""+floatToInteger);
        double integerToDouble=(double) a;
        System.out.println("integerToDouble="+""+integerToDouble);
        float integerToFloat=(float) a;
        System.out.println("integerToFloat="+""+integerToFloat);







    }
}
