import java.io.Serializable;

/**
 * Created by olvet on 05/05/2016.
 */
public class EmployeeSalaryCalculate implements Serializable {
    public byte version = 100;
    public byte count = 0;
    String Name;
    private double rate;
    private int monthsHours;
    private int bonus;
    public EmployeeSalaryCalculate(double rate, int monthsHours, int bonus, String Name) {
        this.rate = rate;
        this.monthsHours = monthsHours;
        this.bonus = bonus;
        this.Name = Name;
    }

    public void setMonthsHours(int monthsHours) {
        this.monthsHours = monthsHours;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getName() {
        return Name;
    }

    public double getRate() {
        return rate;
    }

    public int getMonthsHours() {
        return monthsHours;
    }



    double SalaryCalculateWithoutTax() {
        double salarywithouttax;
        if (monthsHours > 200) {
            salarywithouttax = rate * monthsHours * bonus;
        } else salarywithouttax = rate * monthsHours;
        return salarywithouttax;
    }



    double SalaryCalculateWithTax(double taxrate, double salarywithouttax ) {
        double salarywithtax;
        salarywithtax =( salarywithouttax*(1.0-(taxrate/100)));
        return salarywithtax;

    }

}

	