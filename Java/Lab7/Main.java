import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by olvet on 05/05/2016.
 */
public class Main {
    public static void main(String args[]) throws Exception {
        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        EmployeeSalaryCalculate out = new EmployeeSalaryCalculate(100, 182, 10, "Rodney Miller");
        oos.writeObject(out);
        oos.flush();
        oos.close();
        FileInputStream fis = new FileInputStream("temp.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        EmployeeSalaryCalculate in = (EmployeeSalaryCalculate) oin.readObject();
        System.out.println("version=" + in.version);
        System.out.println();
        double salarydoublewithouttax = in.SalaryCalculateWithoutTax();
        System.out.println(in.Name + " " + "salary without Tax after Serialization=" + salarydoublewithouttax);
        System.out.println(in.Name + " " + "salary with Tax after Serialization=" + in.SalaryCalculateWithTax(13, salarydoublewithouttax));
        Class c = in.getClass();
        Class[] paramTypes = new Class[]{double.class};
        Method m = c.getMethod("setRate", paramTypes);
        Method method = c.getMethod("setRate", paramTypes);
        Object[] arg = new Object[]{new Integer(50)};
        method.invoke(in, arg);
        salarydoublewithouttax = in.SalaryCalculateWithoutTax();
        System.out.println();
        System.out.println(in.Name + " " + "salary without Tax after reflection update=" + salarydoublewithouttax);
        System.out.println(in.Name + " " + "salary with Tax after reflection update=" + in.SalaryCalculateWithTax(13, salarydoublewithouttax));


    }

}
