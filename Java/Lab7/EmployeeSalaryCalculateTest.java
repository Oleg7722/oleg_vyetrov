

import org.junit.runner.JUnitCore;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Created by olvet on 06/05/2016.
 */
public class EmployeeSalaryCalculateTest {

    @org.junit.Test
    public void salaryCalculateWithoutTax() throws Exception {
        EmployeeSalaryCalculate testsalary = new EmployeeSalaryCalculate(100, 182, 10, "Rodney Miller");

        assertEquals("Salary without  Tax of rate=100, monthsHours=182 and bonus=10 should be  18200", 18200.0, testsalary.SalaryCalculateWithoutTax());
        System.out.println("1st");
    }

    @org.junit.Test
    public void salaryCalculateWithTax() throws Exception {
        EmployeeSalaryCalculate testsalary = new EmployeeSalaryCalculate(100, 182, 10, "Rodney Miller");
        double salarywithouttax = testsalary.SalaryCalculateWithoutTax();
        assertEquals("Salary without  Tax of rate=100, monthsHours=182 and bonus=10 should be  15834", 15834.0, testsalary.SalaryCalculateWithTax(13.0, salarywithouttax));
        System.out.println("2nd");
    }

    @org.junit.Test
    public void testTrue() {
        EmployeeSalaryCalculate testsalary = new EmployeeSalaryCalculate(100, 201, 10, "Rodney Miller");
        assertTrue(testsalary.getMonthsHours() > 200 == true);
    }

    public static void main(String[] args) {
        JUnitCore core = new JUnitCore();
        core.run(EmployeeSalaryCalculateTest.class);
    }
}




