import static java.lang.Math.sqrt;

/**
 * Created by Oleg on 17.04.2016.
 */
public class Renault extends Cars {
    double maxSpeed = 0;
    double totaltime=0;
    double v0=0;
    double maxSpeedInMeterPerSecond = (maxSpeed * 1000) / 3600;
    double acceleration = 0;

    public Renault (double acceleration, double maxSpeed, double mobilityFactor, String name  ) {
        this.acceleration = acceleration;
        this.maxSpeed = maxSpeed;
        this.mobilityFactor = mobilityFactor;
        this.name = name;
    }

    public void timeOfRace() {
        double a = acceleration;

        for (int i = 0; i < numberOfLaps+1; i++) {
            v[i] = sqrt(Math.pow(v0, 2) + (2 * a * lengthOfLap));
            t[i] = (v[i] - v0) / a;

            if (v0>=maxSpeedInMeterPerSecond) {
                maxSpeedInMeterPerSecond=maxSpeedInMeterPerSecond*1.1;
                v0=maxSpeedInMeterPerSecond;
            }


            v0 = v[i] * mobilityFactor;
            totaltime = totaltime + t[i];
        }
        this.time=totaltime;
            /* For testing
            System.out.println("v[" + i + "]=" + v[i] + "m/s" + " " + "v[" + i + "]=" + v[i] * 3.6 + "km/h");

            System.out.println("t[" + i + "]=" + t[i]);
            System.out.println(maxSpeedInMeterPerSecond * 3.6);


        }
        System.out.println("totaltime=" + totaltime);
        */
    }
}
