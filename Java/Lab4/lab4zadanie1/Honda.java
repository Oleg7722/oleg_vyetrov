import static java.lang.Math.sqrt;

/**
 * Created by Oleg on 17.04.2016.
 */
public class Honda extends Cars {
    double maxSpeed = 0;
    double totaltime=0;
    double v0=0;
    double maxSpeedInMeterPerSecond = (maxSpeed * 1000) / 3600;
    double acceleration = 0;

    public Honda (double acceleration, double maxSpeed, double mobilityFactor, String name  ) {
        this.acceleration = acceleration;
        this.maxSpeed = maxSpeed;
        this.mobilityFactor = mobilityFactor;
        this.name = name;
    }
    public void timeOfRace() {
        double a = acceleration;

        for (int i = 1; i < numberOfLaps + 1; i++) {
            v[i] = sqrt(Math.pow(v0, 2) + (2 * a * lengthOfLap));
            t[i] = (v[i] - v0) / a;
            if (v0 >= maxSpeedInMeterPerSecond) {
                v0 = maxSpeedInMeterPerSecond;
            } else if (v0 > maxSpeedInMeterPerSecond / 2) {
                m = mobilityFactor + (((0.05 * (maxSpeedInMeterPerSecond / 2)) - v0));
            } else m = mobilityFactor;
            v0 = v[i] * m;
            totaltime = totaltime + t[i];
        }
        this.time=totaltime;
        /* For Testing
            System.out.println("v[" + i + "]=" + v[i] + "m/s" + " " + "v[" + i + "]=" + v[i] * 3.6 + "km/h");

            System.out.println("t[" + i + "]=" + t[i]);
            System.out.println(maxSpeedInMeterPerSecond * 3.6);


        }
        System.out.println("totaltime=" + totaltime);
        */
    }
}
