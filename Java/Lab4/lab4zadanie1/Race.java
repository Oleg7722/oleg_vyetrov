/**
 * Created by Oleg on 17.04.2016.
 */
public class Race {
    public static void main (String args[]){
        Ferrari car1=new Ferrari(20, 15, 0.9, "Ferrari");
        Honda car2=new Honda (2, 200, 0.1,"Honda");
        Renault car3=new Renault (7, 100, 0.5, "Renault");

        car1.timeOfRace();
        car2.timeOfRace();
        car3.timeOfRace();
        Cars cars[]=new Cars [3];
        cars[0]=car1;
        cars[1]=car2;
        cars[2]=car3;
        SortAndOuput.sort(cars);
        SortAndOuput.output(cars);

    }

}
