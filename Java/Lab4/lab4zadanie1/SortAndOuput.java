import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by olvet on 19/04/2016.
 */
public class SortAndOuput {
    public static void sort(Cars[] car) {
        int min;
        for (int i = 0; i < car.length - 1; i++) {
            min = i;
            for (int j = i + 1; j < car.length; j++) {
                if ((car[min].time) > car[j].time)
                    min = j;
            }
            Cars temp = car[i];
            car[i] = car[min];
            car[min] = temp;

        }
    }

    public static void output(Cars[] car) {
        for (int i = 0; i < car.length; i++) {
            int secondsInteger = (int) car[i].time;
            SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
            String result = sdf.format(new Date((secondsInteger * 1000)));
            System.out.println("The car"+" "+car[i].name+" "+"ranked"+" "+ "the place number"+" "+(i+1)+" "+"with the result="+result);


        }
    }
}

