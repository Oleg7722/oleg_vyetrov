import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;


import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


/**
 * Created by olvet on 02.05.2016.
 */
public class SearchAndCopy {
    String filenametocopy;

    SearchAndCopy(String filenametocopy) {
        this.filenametocopy = filenametocopy;
    }

    String fileNameWithOutExt(String buf) {
        String fileName = buf;
        int pos = fileName.lastIndexOf(".");
        if (pos > 0) {
            fileName = fileName.substring(0, pos);
        }
        return fileName;
    }

    String fileExt(String buf) {
        String fileext = buf;
        int pos = fileext.lastIndexOf(".");
        if (pos > 0) {
            fileext = fileext.substring(pos + 1);
        }
        return fileext;
    }

    private static void copyFileUsingJava7Files(File source, File dest) throws IOException {
        Files.copy(source.toPath(), dest.toPath(), REPLACE_EXISTING);
    }

    void searchAndCopyUserFile(ArrayList<File> fileArrayList) {
        int seqnumber = 1;
        for (int i = 0; i < fileArrayList.size(); i++) {
            String filename = fileNameWithOutExt(fileArrayList.get(i).getName());
            String fileext = fileExt(fileArrayList.get(i).getName());

            System.out.println();
            if (filename.contains(filenametocopy)) {
                String from = fileArrayList.get(i).getAbsolutePath();
                String createFolder;
                String Parent = fileArrayList.get(i).getParent();
                createFolder = Parent + "\\" + filenametocopy + seqnumber;
                seqnumber++;
                String toCopy = createFolder + "\\" + fileArrayList.get(i).getName();
                File mkdir = new File(createFolder);
                mkdir.mkdir();
                File source = new File(from);
                File dest = new File(toCopy);
                try {
                    copyFileUsingJava7Files(source, dest);
                } catch (IOException e) {
                    System.out.println("Copying error!");
                }


            }
        }


    }
}
