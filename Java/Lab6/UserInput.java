import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Oleg on 03.05.2016.
 */
public class UserInput {
    static String filenametocopy;

    static String userInput() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the name of file to copy");
            filenametocopy = br.readLine();
        } catch (IOException e) {
            System.err.println("Error: " + e);
        }
        return filenametocopy;
    }
}
