import java.io.File;
import java.util.ArrayList;

/**
 * Created by olvet on 03.05.2016.
 */
public class ThreadOne implements Runnable {
    ArrayList<File> listfile = new ArrayList<>();
    String userInput;

    ThreadOne(ArrayList<File> listfile, String userInput) {
        this.listfile = listfile;
        this.userInput = userInput;

    }

    public void run() {
        SearchAndCopy search = new SearchAndCopy(userInput);

        search.searchAndCopyUserFile(listfile);
    }
}
