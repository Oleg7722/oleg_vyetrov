import java.io.File;
import java.util.ArrayList;

/**
 * Created by olvet on 01.05.2016.
 */
public class Threads {

    public static void main(String args[]) {
        DirToArray dirToArray = new DirToArray();
        String dirToRecurse = "E://Test";
        File dir = new File(dirToRecurse);
        ArrayList<File> fileArrayList = new ArrayList<>();
        fileArrayList = dirToArray.fileList(dir);
        String filenametocopyone = UserInput.userInput();
        ThreadOne myThreadyOne = new ThreadOne(fileArrayList, filenametocopyone);
        Thread myThreadOne = new Thread(myThreadyOne);
        myThreadOne.start();
        String filenametocopytwo = UserInput.userInput();
        ThreadOne myThreadyTwo = new ThreadOne(fileArrayList, filenametocopytwo);
        Thread myThreadTwo = new Thread(myThreadyTwo);
        myThreadTwo.start();


    }
}
