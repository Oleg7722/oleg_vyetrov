/**
 * Created by Oleg on 20.06.2016.
 */

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class WeatherApiHandler {
    ArrayList<String> urlList = new ArrayList<String>();
    ArrayList<String> expectedList = new ArrayList<String>();



    public void getDataFromTxtFile() {

        Scanner scanner = null;

        try {
            scanner = new Scanner(new File("src/URLs.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] string = line.split("   ");
            urlList.add(string[0]);
            expectedList.add(string[1]);


        }



    }

    public String getResponse(String inputuri) {
        String response = "";
        try {
            URL url = new URL(inputuri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;

            while ((output = br.readLine()) != null) {
                response = response + output;
            }

            conn.disconnect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public boolean getExpected(String response, String expected) {
        return response.contains(expected);
    }

}
