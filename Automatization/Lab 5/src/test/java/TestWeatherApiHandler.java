import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Created by olvet on 21/06/2016.
 */
public class TestWeatherApiHandler {
    @Test
    public void getDataFromTxtFile() throws Exception {
        WeatherApiHandler weatherApiHandler = new WeatherApiHandler();
        weatherApiHandler.getDataFromTxtFile();
        for (int i = 0; i < weatherApiHandler.urlList.size(); i++) {
            String response = weatherApiHandler.getResponse(weatherApiHandler.urlList.get(i));
            String expected = weatherApiHandler.expectedList.get(i);
            assertTrue("Expected" + " " + expected + " " + "in" + " " + weatherApiHandler.urlList.get(i) + " " + "is not found", weatherApiHandler.getExpected(response, expected));


        }

    }


}