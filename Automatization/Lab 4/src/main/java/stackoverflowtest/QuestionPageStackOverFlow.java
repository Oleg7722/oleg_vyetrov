package stackoverflowtest;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by olvet on 31/05/2016.
 */
public class QuestionPageStackOverFlow {
    public WebDriver driver;

    public QuestionPageStackOverFlow(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='qinfo']//b[text()='today']")
    WebElement txt_module_question_stats_today;

    public void checkQuestionIsToday() {
        assertTrue("The question is asked not today", txt_module_question_stats_today.isDisplayed());
    }
}