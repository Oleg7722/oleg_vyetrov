package stackoverflowtest;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by olvet on 31/05/2016.
 */
public class SignUpStackOverFlow {
    public WebDriver driver;
    public SignUpStackOverFlow(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@id='hlogo']/a")                              // Sign up pop-up
    public WebElement lnk_topbar_main_page;

    public MainPageStackOverFlow navigatesToMainPage() {
        lnk_topbar_main_page.click();
        return new MainPageStackOverFlow(driver);
    }
    @FindBy (xpath=".//*[@id='openid-buttons']//div[@class='text']/span[text()='Google']|.//*[@id='openid-buttons']//div[@class='text']/span[text()='Facebook']")
    WebElement btn_openid_buttons_google_facebook_buttons;
    public void checkGoogleAndFacebookButtons(){
        assertTrue("Cities links are not displayed", btn_openid_buttons_google_facebook_buttons.isDisplayed());

    }
}
