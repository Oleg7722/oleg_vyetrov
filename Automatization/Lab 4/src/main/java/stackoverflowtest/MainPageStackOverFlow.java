package stackoverflowtest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * Created by olvet on 31/05/2016.
 */
public class MainPageStackOverFlow {
    public WebDriver driver;


    public MainPageStackOverFlow(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//span[@class='bounty-indicator-tab']")
    public WebElement txt_mainbar_number; //Featured Number

    public void checkFeaturedNumber() {
        String stringcount = txt_mainbar_number.getText();
        int count = Integer.parseInt(stringcount);
        assertTrue("Count is less than 300", count > 300);
    }

    @FindBy(xpath = ".//a[text()='sign up']")                              // Sign up pop-up
    public WebElement lnk_topbar_links_sign_up;

    public SignUpStackOverFlow navigatesToSignUp() {
        lnk_topbar_links_sign_up.click();
        return new SignUpStackOverFlow(driver);
    }


    public QuestionPageStackOverFlow navigatesToQuestionPage() {
        int x = (int) (Math.random() * 97) + 1;
        String xPathForQuestion = ".//*[@id='question-mini-list']/div[" + x + "]//a[@class='question-hyperlink']";  // Random Question page
        WebElement lnk_question_mini_list_question = driver.findElement(By.xpath(xPathForQuestion));
        lnk_question_mini_list_question.click();
        return new QuestionPageStackOverFlow(driver);


    }
}