package rozetkatest;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by olvet on 31/05/2016.
 */
public class CartPopUpRozetka {
    public WebDriver driver;

    public CartPopUpRozetka(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='drop-block']/h2[text()='Корзина пуста']")
    WebElement txt_cart_popup_empty;

    public void checkEmptyCartRozetka() {
        assertTrue("Cart is not empty", txt_cart_popup_empty.isDisplayed());

    }
}

