package rozetkatest;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by olvet on 31/05/2016.
 */
public class CitiesPopUpRozetka {
    public WebDriver driver;

    public CitiesPopUpRozetka(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath=".//*[@class='header-city-i']/child::*[contains(text(), 'Киев')]")
    WebElement lnk_city_kiev;
    @FindBy(xpath=".//*[@class='header-city-i']/child::*[contains(text(), 'Харьков')]")
    WebElement lnk_city_kharkov;
    @FindBy(xpath=".//*[@class='header-city-i']/child::*[contains(text(), 'Одесса')]")
    WebElement lnk_city_odesssa;


    public void checkCitiesRozetka() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        assertTrue("Cities links are not displayed", lnk_city_kiev.isDisplayed()&&lnk_city_kharkov.isDisplayed()&&lnk_city_odesssa.isDisplayed());

    }

}

