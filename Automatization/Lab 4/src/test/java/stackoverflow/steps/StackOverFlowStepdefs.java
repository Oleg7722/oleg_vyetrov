package stackoverflow.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import runner.Runner;
import static runner.Runner.driver;
import static runner.Runner.mainPageStackOverFlow;
import static runner.Runner.signUpStackOverFlow;
import static runner.Runner.questionPageStackOverFlow;


import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;



/**
 * Created by olvet on 04/06/2016.
 */
public class StackOverFlowStepdefs {
    @Given("^I on StackOverflow start page$")
    public void iOnStackOverflowStartPage() throws Throwable {
        Runner.driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

    }

    @Then("^I see that featured count is more than (\\d+)$")
    public void iSeeThatFeaturedCountIsMoreThan(int arg0) throws Throwable {
        mainPageStackOverFlow.checkFeaturedNumber();

    }

    @When("^I click Sign Up link$")
    public void iClickSignUpLink() throws Throwable {
        signUpStackOverFlow=mainPageStackOverFlow.navigatesToSignUp();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

    }

    @Then("^I see that Google and Facebook buttons are present on Sign Up page$")
    public void iSeeThatGoogleAndFacebookButtonsArePresentOnSignUpPage() throws Throwable {
        signUpStackOverFlow.checkGoogleAndFacebookButtons();

    }


    @When("^I click on random question$")
    public void iClickOnRandomQuestion() throws Throwable {
        questionPageStackOverFlow=mainPageStackOverFlow.navigatesToQuestionPage();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

    }

    @Then("^I see that date is today$")
    public void iSeeThatDateIsToday() throws Throwable {
        questionPageStackOverFlow.checkQuestionIsToday();
    }


}

