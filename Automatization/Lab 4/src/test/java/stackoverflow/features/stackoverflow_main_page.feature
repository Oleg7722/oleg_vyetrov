@all_tests_stackoverflow
Feature: Test Stackoverflow main page
  @stackoverflow_featured_count
  Scenario: Check featured count is more than 300
    Given I on StackOverflow start page
    Then I see that featured count is more than 300

    @stackoverflow_sign_up
  Scenario: Check Google and Facebook buttons are present on Sign Up page
    Given I on StackOverflow start page
    When  I click Sign Up link
    Then I see that Google and Facebook buttons are present on Sign Up page


  @stackoverflow_random_question_is_today
  Scenario: Check Random_question_is_today
    Given I on StackOverflow start page
    When  I click on random question
    Then I see that date is today


