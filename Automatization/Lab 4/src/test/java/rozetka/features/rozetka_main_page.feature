@all_tests_rozetka
Feature: Test rozetka main page
  @rozetka_logo

  Scenario: Check Rozetka logo is present
    Given I on Rozetka start page
    Then I see Rozetka logo

  @rozetka_apple_link

  Scenario: Check Apple  link is present
    Given I on Rozetka start page

    Then I see Apple  link

  @rozetka_mp3_term

  Scenario: Check MP3 term is present in menu section
    Given I on Rozetka start page
    Then I see MP3 term is present in menu section

  @rozetka_select_cities

  Scenario: Check that given cities are displayed in "Выберите свой город" section
    Given I on Rozetka start page
    When  I click "Выберите город" link
    Then I see "Харьков", "Одесса" and "Киев" are displayed in the section

  @rozetka_empy_cart

  Scenario: Check that Cart is empty
    Given I on Rozetka start page
    When  I click Cart link
    Then I check if  Cart is empty





