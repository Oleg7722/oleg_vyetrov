 package rozetka.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import rozetkatest.MainPageRozetka;
import runner.Runner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static runner.Runner.driver;
import static runner.Runner.mainPageRozetka;

import static runner.Runner.cartPopUpRozetka;
import static runner.Runner.citiesPopUpRozetka;

 /**
 * Created by olvet on 04/06/2016.
 */
public class RozetkaStepdefs {

     @Given("^I on Rozetka start page$")
    public void iOnRozetkaStartPage() throws Throwable {
         Runner.driver.get("https://rozetka.com.ua/");
         driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);


    }


    @Then("^I see Rozetka logo$")
    public void iSeeRozetkaLogo() throws Throwable {
        mainPageRozetka.checkLogoRozetka();

    }


    @Then("^I see Apple  link$")
    public void iSeeAppleLink() throws Throwable {
        mainPageRozetka.checkLinkApple();

    }

    @Then("^I see MP(\\d+) term is present in menu section$")
    public void iSeeMPTermIsPresentInMenuSection(int arg0) throws Throwable {
        mainPageRozetka.checkWordMp3();


    }

    @When("^I click \"([^\"]*)\" link$")
    public void iClickLink(String arg0) throws Throwable {
        citiesPopUpRozetka=mainPageRozetka.navigatesToCities();

    }

    @Then("^I see \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" are displayed in the section$")
    public void iSeeAndAreDisplayedInTheSection(String arg0, String arg1, String arg2) throws Throwable {
        citiesPopUpRozetka.checkCitiesRozetka();
    }

     @When("^I click Cart link$")
     public void iClickCartLink() throws Throwable {
         cartPopUpRozetka=mainPageRozetka.navigatesToCart();

     }

    @Then("^I check if  Cart is empty$")
    public void iCheckIfCartIsEmpty() throws Throwable {
        cartPopUpRozetka.checkEmptyCartRozetka();

    }


}
