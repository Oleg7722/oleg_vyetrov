package runner;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;




import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import rozetkatest.*;
import stackoverflowtest.*;

@RunWith(Cucumber.class)

/**
 * Created by student on 6/1/2016.
 */
@CucumberOptions(
        plugin={"pretty", "html:target/cucumber"},
        features={ "src/test/java/rozetka/features","src/test/java/stackoverflow/features" },
        glue={ "rozetka/steps", "stackoverflow/steps"},//адрес к папке с тестами начиная с корневого пакета
        tags={   "@all_tests_rozetka, @all_tests_stackoverflow"  }
)
public class Runner {
    public static WebDriver driver=new FirefoxDriver();
    public static MainPageRozetka mainPageRozetka;
    public static CartPopUpRozetka cartPopUpRozetka;
    public static CitiesPopUpRozetka citiesPopUpRozetka;
    public static MainPageStackOverFlow mainPageStackOverFlow;
    public static SignUpStackOverFlow signUpStackOverFlow;
    public static QuestionPageStackOverFlow questionPageStackOverFlow;


    @BeforeClass
    public static void BeforeClass(){

    mainPageRozetka=new MainPageRozetka(driver);
       mainPageStackOverFlow=new MainPageStackOverFlow(driver);




    }
    @AfterClass
    public static void afterClass(){
        driver.close();



    }


}

