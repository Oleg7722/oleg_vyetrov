/**
 * Created by olvet on 30/05/2016.
 */

import org.junit.After;
import org.junit.AfterClass;
import rozetka.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class TestRozetka {
    private static WebDriver driver;
    protected static MainPageRozetka mainPageRozetka;
    protected CitiesPopUpRozetka citiesPopUpRozetka;
    protected CartPopUpRozetka cartPopUpRozetka;

    @BeforeClass
    public static void SetUp() {
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        mainPageRozetka = new MainPageRozetka(driver);
    }

    @Test
    public void checkLogoRozetkaTest() throws Exception {             //Checking Rozetka logo
        mainPageRozetka.checkLogoRozetka();
    }

    @Test
    public void checkAppleLinkTest() throws Exception {               // Checking Apple link
        mainPageRozetka.checkLinkApple();
    }

    @Test
    public void checkCitiesRozetkaTest() throws Exception {           // Checking cities in a select cities Pop-Up
        citiesPopUpRozetka = mainPageRozetka.navigatesToCities();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        citiesPopUpRozetka.checkCitiesRozetka();

    }

    @Test
    public void checkEmptyCartTest() throws Exception {           // Checking cities in a select cities Pop-Up
        cartPopUpRozetka = mainPageRozetka.navigatesToCart();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        cartPopUpRozetka.checkEmptyCartRozetka();

    }

    @AfterClass
    public static void afterSetUp() throws Exception {
        driver.quit();
    }
}