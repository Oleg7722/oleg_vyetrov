/**
 * Created by olvet on 31/05/2016.
 */
import org.junit.AfterClass;
import stackoverflow.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import java.util.concurrent.TimeUnit;

public class TestStackOverFlow {
    private static WebDriver driver;
    protected static MainPageStackOverFlow mainPageStackOverFlow;
    protected SignUpStackOverFlow signUpStackOverFlow;
    protected QuestionPageStackOverFlow questionPageStackOverFlow;

    @BeforeClass
    public static void SetUp() {
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        mainPageStackOverFlow = new MainPageStackOverFlow(driver);
    }

    @Test
    public void checkFeaturedNumberTest() throws Exception {             //Checking Rozetka logo
        mainPageStackOverFlow.checkFeaturedNumber();
    }

    @Test
    public void checkSignUpTest() throws Exception {           // Checking Google and Facebook buttons in Sign Up
        signUpStackOverFlow = mainPageStackOverFlow.navigatesToSignUp();
        signUpStackOverFlow.checkGoogleAndFacebookButtons();
        signUpStackOverFlow.navigatesToMainPage();
    }
    @Test
    public void checkQuestionDateTest() {           // Checking cities in a select cities Pop-Up
        questionPageStackOverFlow = mainPageStackOverFlow.navigatesToQuestionPage();
        questionPageStackOverFlow.checkQuestionIsToday();
    }

    @AfterClass
    public static void afterSetUp() throws Exception {
        driver.quit();
    }
}