package rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by olvet on 30/05/2016.
 */
public class MainPageRozetka {
    public WebDriver driver;

    public MainPageRozetka(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*/div[@class='logo']/img[@title='Интернет магазин Rozetka.ua™ - №1']")
    public WebElement img_body_header_logo; //Rozetka logo


    public void checkLogoRozetka() {
        assertTrue("Rozetka logo is not displayed", img_body_header_logo.isDisplayed());
    }

    @FindBy(xpath = ".//li[@class='m-main-l-i']/a[contains(text(),'Apple')]")
    public WebElement lnk_m_main_apple_link; //Apple  link

    public void checkLinkApple() {
        assertTrue("Link'Apple' is not displayed", lnk_m_main_apple_link.isDisplayed());
    }

    @FindBy(xpath = ".//*[@id='city-chooser']/a")                              // Cities pop up
    public WebElement lnk_header_city_select_cities;

    public CitiesPopUpRozetka navigatesToCities() {
        lnk_header_city_select_cities.click();
        return new CitiesPopUpRozetka(driver);
    }

    @FindBy(xpath = ".//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    // Cart popup
    public WebElement lnk_cart_popup_cart;

    public CartPopUpRozetka navigatesToCart() {
        lnk_cart_popup_cart.click();
        return new CartPopUpRozetka(driver);
    }
}




