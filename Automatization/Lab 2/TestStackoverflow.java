import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by olvet on 30/05/2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private static File pathBinary;
    private static FirefoxBinary Binary;
    private static FirefoxProfile firefoxPro;
    String URL;

    @BeforeClass
    public static void beforeClassSetUp() throws Exception {


    }
    @Before
    public void beforeSetUp() throws Exception {
        pathBinary = new File("C:\\Program Files (x86)\\Mozilla Firefox38\\firefox.exe");
        Binary = new FirefoxBinary(pathBinary);
        driver = new FirefoxDriver(Binary, firefoxPro);
        driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();


    }




    @Test
    public void featuredNumberTest() throws Exception {
        String stringcount=driver.findElement(By.xpath(".//span[@class='bounty-indicator-tab']")).getText();
        int count=Integer.parseInt(stringcount);
        assertTrue("Count is less than 300", count>300);
    }

    @Test
    public void signUpTest() throws Exception {
        driver.findElement(By.xpath(".//a[text()='sign up']")).click();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        String xPathForGoogleAndFacebook=".//*[@id='openid-buttons']//div[@class='text']/span[text()='Google']|.//*[@id='openid-buttons']//div[@class='text']/span[text()='Facebook']";
        assertTrue("Google and Facebook buttons are not displayed", driver.findElement(By.xpath(xPathForGoogleAndFacebook)).isDisplayed());
    }
    @Test
    public void questionTodayTest() throws Exception {
        int questionIndex= (int)(Math.random()*97)+1;
        String xPathForQuestion=".//*[@id='question-mini-list']/div["+questionIndex+"]//a[@class='question-hyperlink']";
        driver.findElement(By.xpath(xPathForQuestion)).click();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        String xPathForDate=".//*[@id='qinfo']//b[text()='today']";
        assertTrue("The question is asked not today", driver.findElement(By.xpath(xPathForDate)).isDisplayed());
    }






    @After
    public void afterSetUp() throws Exception {
        driver.quit();

    }

}
