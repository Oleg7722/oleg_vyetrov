import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;

import javax.xml.ws.soap.Addressing;
import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by olvet on 28/05/2016.
 */


public class TestRozetka {
    private static WebDriver driver;
    private static File  pathBinary;
    private static FirefoxBinary Binary;
    private static FirefoxProfile firefoxPro;
    String URL;

    @BeforeClass
    public static void beforeClassSetUp() throws Exception {


    }
    @Before public void beforeSetUp() throws Exception {
        pathBinary = new File("C:\\Program Files (x86)\\Mozilla Firefox38\\firefox.exe");
        Binary = new FirefoxBinary(pathBinary);
        driver = new FirefoxDriver(Binary, firefoxPro);
        driver.get("http://rozetka.com.ua");
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();


    }




    @Test
    public void btestLogoRozetka() throws Exception {
        assertTrue("Rozetka logo is not displayed", driver.findElement(By.xpath(".//*/div[@class='logo']/img[@title='Интернет магазин Rozetka.ua™ - №1']")).isDisplayed());
    }

    @Test
    public void testLinkApple() throws Exception {
        assertTrue("Link'Apple' is not displayed", driver.findElement(By.xpath(".//li[@class='m-main-l-i']/a[contains(text(),'Apple')]")).isDisplayed());
    }

    @Test
    public void testElementMP3() throws Exception {
        assertTrue("Element 'MP3' is not displayed", driver.findElement(By.xpath(".//li[@class='m-main-l-i']/a[contains(text(),'MP3')]")).isDisplayed());
    }

    @Test
    public void atestHeaderLinks() throws Exception {
        driver.findElement(By.xpath(".//*[@id='city-chooser']/a")).click();

        String xPathForCities = ".//*[@class='header-city-i']/child::*[contains(text(), 'Одесса')]|.//*[@class='header-city-i']/child::*[contains(text(), 'Киев')]|.//*[@class='header-city-i']/child::*[contains(text(), 'Харьков')]";

        assertTrue("Cities links are not displayed", driver.findElement(By.xpath(xPathForCities)).isDisplayed());


    }
    @Test
    public void ztestEmptyBucket() throws Exception {
        driver.findElement(By.xpath(".//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")).click();
        assertTrue("Bucket is not empty", driver.findElement(By.xpath(".//*[@id='drop-block']/h2[text()='Корзина пуста']")).isDisplayed());



    }
    @After public void afterSetUp() throws Exception {
        driver.quit();

    }


}